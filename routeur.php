<?php

require_once('controleur/' . $gestion . 'Controleur.php');
if (isset($_REQUEST['action'])) {

    $action = $_REQUEST['action'];

    switch ($action) {
        case 'consulter':
            consulter($_REQUEST);
            break;
        case 'modifier':
            modifier($_REQUEST);
            break;
        case 'supprimer':
            supprimer($_REQUEST);
            break;
        case 'ajouter':
            ajouter($_REQUEST);
            break;
        case 'validationAjout':
            validationAjout($_REQUEST);
            break;
        case 'validationModif':
            validationModif($_REQUEST);
            break;
        case 'validationSuppr':
            validationSuppr($_REQUEST);
            break;
    break;}
} else  {
    lister();
} 