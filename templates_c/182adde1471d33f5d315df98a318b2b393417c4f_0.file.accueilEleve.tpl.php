<?php
/* Smarty version 3.1.29, created on 2018-03-02 14:29:55
  from "C:\xampp\htdocs\Site_no_authentification\vue\accueilEleve.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a9951d395ef41_78410792',
  'file_dependency' => 
  array (
    '182adde1471d33f5d315df98a318b2b393417c4f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Site_no_authentification\\vue\\accueilEleve.tpl',
      1 => 1519135452,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a9951d395ef41_78410792 ($_smarty_tpl) {
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="template/style.css">
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="stylesheet" href="template/head&foot.css">
        <link rel="stylesheet" href="template/bootstrap.min.css">
        <link rel="icon" type="image/png" href="template/image/favicon.ico.png">
        <title><?php echo $_smarty_tpl->tpl_vars['titreForm']->value;?>
</title>
    </head>
    <body>
        <header>
            <h1>
                Pixel Drool
                <img alt="Logo de l'auto-école Pixel Drool" src="template/image/logoPixelDrool.png" class="img-logo">
            </h1>
            <nav>
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="#">Liste des élèves</a>
                    </li>
                    <li>
                        <a href="#">Liste des séances</a>
                    </li>

                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control">
                        </div>
                    </form>
                    <button type="submit" class="btn btn-default">
                        Rechercher
                    </button>
            </nav>
        </header>

        <h3><?php echo $_smarty_tpl->tpl_vars['titreForm']->value;?>
</h3>
        <div id="divTab"
             <form class="tableau" action="index.php" method="POST">

                <table>
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Début</th>
                            <th>Durée</th>
                            <th>Commentaire</th>
                            <th>Fonction</th>
                        </tr>

                    </thead>
                    <?php
$_from = $_smarty_tpl->tpl_vars['accueilEleve']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_SeanceEleve_0_saved_item = isset($_smarty_tpl->tpl_vars['SeanceEleve']) ? $_smarty_tpl->tpl_vars['SeanceEleve'] : false;
$_smarty_tpl->tpl_vars['SeanceEleve'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['SeanceEleve']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['SeanceEleve']->value) {
$_smarty_tpl->tpl_vars['SeanceEleve']->_loop = true;
$__foreach_SeanceEleve_0_saved_local_item = $_smarty_tpl->tpl_vars['SeanceEleve'];
?>
                        <tbody>
                            <tr>
                                <td><span><?php echo $_smarty_tpl->tpl_vars['libDateSeance']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['SeanceEleve']->value['DateSeance'];?>
</td>
                                <td><span><?php echo $_smarty_tpl->tpl_vars['libHeureDebut']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['SeanceEleve']->value['HeureDebut'];?>
</td>
                                <td><span><?php echo $_smarty_tpl->tpl_vars['libDuree']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['SeanceEleve']->value['Duree'];?>
</td>
                                <td><span><?php echo $_smarty_tpl->tpl_vars['libCommentaire']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['SeanceEleve']->value['Commentaire'];?>
</td>
                                <td class="der">
                                    <form action="" method="GET">
                                        <input type="submit" name="consult" value="C" class="sub">
                                        <input type='hidden' name='consult' value='' class="sub">
                                    </form>
                                    <form action="" method="POST">
                                        <input type="submit" name="modif" value="M" class="sub">
                                        <input type='hidden' name='modif' value='' class="sub">
                                    </form>
                                    <form action="index.php" method="POST">
                                        <input type="submit" name="supp" onclick="return confirm('Merci de confirmer la suppression')" value="S" class="sub">
                                        <input type='hidden' name='supp' value="$accueilEleve['IDIndividu']" class="sub">
                                    </form>
                                    <!-- <form action="ajout.php" method="POST">
                                         <input type="submit" name="ajout"  value="+" class="sub">
                                         <input type='hidden' name='ajout' value='' class="sub">
                                     </form>-->
                                </td>
                            </tr>
                        </tbody>
                    <?php
$_smarty_tpl->tpl_vars['SeanceEleve'] = $__foreach_SeanceEleve_0_saved_local_item;
}
if ($__foreach_SeanceEleve_0_saved_item) {
$_smarty_tpl->tpl_vars['SeanceEleve'] = $__foreach_SeanceEleve_0_saved_item;
}
?>
                </table>
            </form>
        </div>
        <form>
            <a href=""><input type="submit" name="Retour" value="Retour" class="clicc"></a>
        </form>

    </body>
</html>
<?php }
}
