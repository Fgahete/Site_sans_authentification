<?php
/* Smarty version 3.1.29, created on 2018-03-04 12:56:59
  from "C:\xampp\htdocs\Site_no_authentification\vue\accueilMoniteur.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a9bdf0beb00e4_37133840',
  'file_dependency' => 
  array (
    'b91e8f285792531f15f666690ba486d337538526' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Site_no_authentification\\vue\\accueilMoniteur.tpl',
      1 => 1520163279,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a9bdf0beb00e4_37133840 ($_smarty_tpl) {
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="template/style.css">
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="stylesheet" href="template/head&foot.css">
        <link rel="stylesheet" href="template/bootstrap.min.css">
        <link rel="icon" type="image/png" href="template/image/favicon.ico.png">
        <title><?php echo $_smarty_tpl->tpl_vars['titreForm']->value;?>
</title>
    </head>
    <body>
        <header>
            <h1>
                Pixel Drool
                <img alt="Logo de l'auto-école Pixel Drool" src="template/image/logoPixelDrool.png" class="img-logo">
            </h1>
            <nav><section class="lienacc">
                 <a href="index.php?gestion=admin">Admin</a>
                <a href="index.php?gestion=moniteur">Moniteur</a>
                 <a href="index.php?gestion=eleve">Eleve</a></section>
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="#">Liste des élèves</a>
                    </li>
                    <li>
                        <a href="index.php?gestion=moniteur">Liste des séances</a>
                    </li>
                </ul>
                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control">
                        </div>
                    </form>
                    <button type="submit" class="btn btn-default">
                        Rechercher
                    </button>
            </nav>
        </header>

        <h3><?php echo $_smarty_tpl->tpl_vars['titreForm']->value;?>
</h3>
        <form action='index.php' method='GET'>
                <input value="moniteur" name="gestion" type="hidden">
                <input value="ajouter" name="action" type="hidden">
                <input type='submit' name='ajouter' value='Ajouter'><br><br><br>
            </form>
        <div id="divTab"
             <form class="tableau" action="index.php" method="POST">

                <table>
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Début</th>
                            <th>Durée</th>
                            <th></th>
                        </tr>

                    </thead>
                    <?php
$_from = $_smarty_tpl->tpl_vars['accueilMoniteur']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_moniteur_0_saved_item = isset($_smarty_tpl->tpl_vars['moniteur']) ? $_smarty_tpl->tpl_vars['moniteur'] : false;
$_smarty_tpl->tpl_vars['moniteur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['moniteur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['moniteur']->value) {
$_smarty_tpl->tpl_vars['moniteur']->_loop = true;
$__foreach_moniteur_0_saved_local_item = $_smarty_tpl->tpl_vars['moniteur'];
?>
                        <tbody>
                            <tr>
                                <td><span><?php echo $_smarty_tpl->tpl_vars['libdateseance']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['moniteur']->value['dateseance'];?>
</td>
                                <td><span><?php echo $_smarty_tpl->tpl_vars['libnom']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['moniteur']->value['nom'];?>
</td>
                                <td><span><?php echo $_smarty_tpl->tpl_vars['libprenom']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['moniteur']->value['prenom'];?>
</td>
                                <td><span><?php echo $_smarty_tpl->tpl_vars['libheuredebut']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['moniteur']->value['heuredebut'];?>
</td>
                                <td><span><?php echo $_smarty_tpl->tpl_vars['libduree']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['moniteur']->value['duree'];?>
</td>
                               <td class="der">
                                     <form class="enligne" action="index.php" method="POST">
                           <input value="<?php echo $_smarty_tpl->tpl_vars['moniteur']->value['idseance'];?>
" name="ids" type="hidden">
                            <input value="<?php echo $_smarty_tpl->tpl_vars['moniteur']->value['idindividu'];?>
" name="id" type="hidden">
                            <input value="moniteur" name="gestion" type="hidden">
                            <input value="consulter" name="action" type="hidden">
                            <input value="consulter" name="consulter" type="submit">
                        </form><br>
                        <form class="enligne" action="index.php" method="POST">
                              <input value="<?php echo $_smarty_tpl->tpl_vars['moniteur']->value['idseance'];?>
" name="ids" type="hidden">
                            <input value="<?php echo $_smarty_tpl->tpl_vars['moniteur']->value['idindividu'];?>
" name="id" type="hidden">
                            <input value="moniteur" name="gestion" type="hidden">
                            <input value="modifier" name="action" type="hidden">
                            <input value="modifier" name="modifier" type="submit">
                            </form><br>
                        
                        <form class="enligne" action="index.php" method="POST">
                              <input value="<?php echo $_smarty_tpl->tpl_vars['moniteur']->value['idseance'];?>
" name="ids" type="hidden">
                            <input value="<?php echo $_smarty_tpl->tpl_vars['moniteur']->value['idindividu'];?>
" name="id" type="hidden">
                            <input value="moniteur" name="gestion" type="hidden">
                            <input value="supprimer" name="action" type="hidden">
                            <input value="supprimer" name="supprimer" type="submit">
                        </form><br>
                                   
                                </td>
                            </tr>
                        </tbody>
                    <?php
$_smarty_tpl->tpl_vars['moniteur'] = $__foreach_moniteur_0_saved_local_item;
}
if ($__foreach_moniteur_0_saved_item) {
$_smarty_tpl->tpl_vars['moniteur'] = $__foreach_moniteur_0_saved_item;
}
?>
                </table>
            </form>
        </div>
        <form>
            <a href=""><input type="submit" name="Retour" value="Retour" class="clicc"></a>
        </form>

    </body>
</html>
<?php }
}
