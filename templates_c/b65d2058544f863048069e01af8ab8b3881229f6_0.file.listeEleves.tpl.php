<?php
/* Smarty version 3.1.29, created on 2018-02-13 14:28:24
  from "C:\xampp\htdocs\PixelDrool\vue\listeEleves.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a82e7f88eef27_52023889',
  'file_dependency' => 
  array (
    'b65d2058544f863048069e01af8ab8b3881229f6' => 
    array (
      0 => 'C:\\xampp\\htdocs\\PixelDrool\\vue\\listeEleves.tpl',
      1 => 1518528503,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a82e7f88eef27_52023889 ($_smarty_tpl) {
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="template/style.css">
        <link rel="stylesheet" href="template/head&foot.css">
        <link rel="stylesheet" href="template/bootstrap.min.css">
        <title><?php echo $_smarty_tpl->tpl_vars['titreForm']->value;?>
</title>
    </head>
    <body>
        <h1>
            Pixel Drool<br>

            Auto-école, sur la route du renard futé.<br>
            <img alt="Logo de l'auto-école Pixel Drool" src="template/image/logoPixelDrool.png" class="img-logo">
        </h1>
        <nav>
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="#">Liste des élèves</a>
                </li>
                <li>
                    <a href="#">Liste des séances</a>
                </li>

                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control">
                    </div> 
                </form>
                <button type="submit" class="btn btn-default">
                    Rechercher
                </button>
        </nav>

        <h3><?php echo $_smarty_tpl->tpl_vars['titreForm']->value;?>
</h3>
        <div id="divTab"
             <form class="tableau" action="index.php" method="POST">
                 <!--Nombre de lignes affichées : <?php echo $_smarty_tpl->tpl_vars['nbLig']->value;?>
<br><br><br>-->

                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Téléphone</th>
                            <th></th>
                        </tr>

                    </thead>
                    <?php
$_from = $_smarty_tpl->tpl_vars['listeEleves']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_eleve_0_saved_item = isset($_smarty_tpl->tpl_vars['eleve']) ? $_smarty_tpl->tpl_vars['eleve'] : false;
$_smarty_tpl->tpl_vars['eleve'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['eleve']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['eleve']->value) {
$_smarty_tpl->tpl_vars['eleve']->_loop = true;
$__foreach_eleve_0_saved_local_item = $_smarty_tpl->tpl_vars['eleve'];
?>
                        <tbody>
                            <tr>
                                <!--<td><span><?php echo $_smarty_tpl->tpl_vars['libVignette']->value;?>
</span> : <?php echo $_smarty_tpl->tpl_vars['eleve']->value['vignette'];?>
</td>-->
                                <td><span><?php echo $_smarty_tpl->tpl_vars['libIDIndividu']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['eleve']->value['IDIndividu'];?>
</td>
                                <td><span><?php echo $_smarty_tpl->tpl_vars['libNom']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['eleve']->value['Nom'];?>
</td>
                                <td><span><?php echo $_smarty_tpl->tpl_vars['libPrenom']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['eleve']->value['Prenom'];?>
</td>
                                <td><span class="telEleve"><?php echo $_smarty_tpl->tpl_vars['libTelephone']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['eleve']->value['NumeroTelephone'];?>
</td>
                                <td class="der">
                                    <form>
                                        <input type="submit" name="consult" value="C" class="sub">
                                        <input type='hidden' name='consult' value='' class="sub">
                                    </form>
                                    <form>
                                        <input type="submit" name="modif" value="M" class="sub">
                                        <input type='hidden' name='modif' value='' class="sub">
                                    </form>
                                    <form>
                                        <input type="submit" name="supp" value="S" class="sub">
                                        <input type='hidden' name='supp' value='' class="sub">
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                    <?php
$_smarty_tpl->tpl_vars['eleve'] = $__foreach_eleve_0_saved_local_item;
}
if ($__foreach_eleve_0_saved_item) {
$_smarty_tpl->tpl_vars['eleve'] = $__foreach_eleve_0_saved_item;
}
?>
                </table> 

            </form>
        </div>
        <form>
            <input type="submit" name="retour" value="Retour" class="clicc">
        </form>

    </body>
</html><?php }
}
