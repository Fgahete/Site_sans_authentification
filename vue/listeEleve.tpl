<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="stylesheet" href="template/style.css">
        <link rel="stylesheet" href="template/head&foot.css">
        <link rel="stylesheet" href="template/bootstrap.min.css">
        <link rel="icon" type="image/png" href="template/image/favicon.ico.png">
        <title>{$titreForm}</title>
    </head>
    <body>
        <header>
            <h1>
                Pixel Drool
                <img alt="Logo de l'auto-école Pixel Drool" src="template/image/logoPixelDrool.png" class="img-logo">
            </h1>
            <nav>
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="#">Liste des élèves</a>
                    </li>
                    <li>
                        <a href="#">Liste des séances</a>
                    </li>

                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control">
                        </div>
                    </form>
                    <button type="submit" class="btn btn-default">
                        Rechercher
                    </button>
            </nav>
        </header>
        <a href="accueilEleve.php">Accueil Eleve</a>

        <h3>{$titreForm}</h3>
        <div id="divTab"
             <form class="tableau" action="index.php" method="POST">

                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Téléphone</th>
                            <th></th>
                        </tr>

                    </thead>
                    {foreach from=$listeEleve item=eleve}
                        <tbody>
                            <tr>
                                <td><span>{$libIDIndividu}</span> {$eleve.IDIndividu}</td>
                                <td><span>{$libNom}</span> {$eleve.Nom}</td>
                                <td><span>{$libPrenom}</span> {$eleve.Prenom}</td>
                                <td><span class="telEleve">{$libTelephone}</span> {$eleve.NumeroTelephone}</td>
                                <td class="der">
                                    <form action="" method="GET">
                                        <input type="submit" name="consult" value="C" class="sub">
                                        <input type='hidden' name='consult' value='' class="sub">
                                    </form>
                                    <form action="" method="POST">
                                        <input type="submit" name="modif" value="M" class="sub">
                                        <input type='hidden' name='modif' value='' class="sub">
                                    </form>
                                    <form action="index.php" method="POST">
                                        <input type="submit" name="supp" onclick="return confirm('Merci de confirmer la suppression')" value="S" class="sub">
                                        <input type='hidden' name='supp' value="$listeEleve['IDIndividu']" class="sub">
                                    </form>
                                    <!-- <form action="ajout.php" method="POST">
                                         <input type="submit" name="ajout"  value="+" class="sub">
                                         <input type='hidden' name='ajout' value='' class="sub">
                                     </form>-->
                                </td>
                            </tr>
                        </tbody>
                    {/foreach}
                </table>
            </form>
        </div>
        <form>
            <a href=""><input type="submit" name="Retour" value="Retour" class="clicc"></a>
        </form>

    </body>
</html>