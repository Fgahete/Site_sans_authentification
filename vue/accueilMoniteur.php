<?php
require_once ('include/libs/smarty.class.php');
$tpl = new Smarty();
// Chargement des données
if (isset($parametre['action'])) {

    $action = $parametre['action'];
    $tpl->assign("action", $action);
    switch ($action) {
        case 'consulter':
            $varLecture = " readonly='readonly' ";

            detailSeance($idRequete, $tpl, $varLecture);
            $tpl->assign("action", $action);
            $tpl->assign("btnAction", "");

            $tpl->display('Vue/accueilMoniteurFiche.tpl');

            break;
        case 'ajouter':
            $varLecture = "";
            formulaire($varLecture, $tpl);
            $tpl->assign("action", 'validationAjout');
            $tpl->assign("btnAction", "<input type='submit' name='Valider' value='Valider'>");
            $tpl->display('Vue/accueilMoniteurFicheModifier.tpl');

            break;

        case 'modifier':
            $varLecture2 = "";
            $varLectureSeul = " readonly='readonly' ";

            detailSeanceModif($idRequete, $tpl, $varLecture2, $varLectureSeul);
            $tpl->assign("action", 'validationModif');
            $tpl->assign("btnAction", "<input type='submit' name='Valider' value='Valider'>");
            $tpl->display('Vue/accueilMoniteurFicheModifier.tpl');

            break;
        case 'supprimer':
            $varLecture = " readonly='readonly' ";

            detailSeance($idRequete, $tpl, $varLecture);
            $tpl->assign("action", 'validationSuppr');
            $tpl->assign("btnAction", "<input type='submit' name='supprimer' value='Supprimer'");

            $tpl->display('Vue/accueilMoniteurFiche.tpl');

            break;
    }
} else {
$accueilMoniteur = array();
$i = 0;
//pas de paramètre dans fetch() ==> tableau associatif. Pour des nombres PDO::FETCH_NUM
while ($row = $idRequete->fetch()) {
      $accueilMoniteur[$i]['idindividu'] = $row['idindividu'];
        $accueilMoniteur[$i]['idseance'] = $row['idseance'];
    $accueilMoniteur[$i]['dateseance'] = $row['dateseance'];
    $accueilMoniteur[$i]['nom'] = $row['nom'];
    $accueilMoniteur[$i]['prenom'] = $row['prenom'];
    $accueilMoniteur[$i]['heuredebut'] = $row['heuredebut'];
    $accueilMoniteur[$i]['duree'] = $row['duree'];
    $i++;
}
$tpl->assign("libdidindividu", "");
$tpl->assign("libidseance", "");
$tpl->assign("libdateseance", "");
$tpl->assign("libnom", "");
$tpl->assign("libprenom", "");
$tpl->assign("libheuredebut", "");
$tpl->assign("libduree", "");

/* $nbLig = $idRequete->rowCount(); */
$tpl->assign('titreForm', 'Liste des séances');
$tpl->assign('accueilMoniteur', $accueilMoniteur);
$tpl->display('vue/accueilMoniteur.tpl');
}



function detailSeance($idRequete, $tpl, $varLecture) {
    $row = $idRequete->fetch();
    $row_id = $row['idindividu'];
    $row_phot = $row['photos'];
    $row_nom = $row['nom'];
    $row_pre = $row['prenom'];
    $row_tel = $row['telephone'];
    $row_mail = $row['mail'];
    $row_dat = $row['dateseance'];
    $row_deb = $row['heuredebut'];
    $row_dur = $row['duree'];
    $row_com = $row['commentaire'];



//Assignation deis valeurs récupérées
    $tpl->assign("titreForm", "Fiche Seance en modification");
    $tpl->assign("id", "<input type='text' name='f_id' value='" . $row_id . "' $varLecture>");
    $tpl->assign("phot", "<input type='image' name='f_phot' value='" . $row_phot . "' $varLecture> ");
    $tpl->assign("nom", "<input type='text' name='f_nom' value='" . $row_nom . "' $varLecture> ");
    $tpl->assign("pre", "<input type='text' name='f_pre' value='" . $row_pre . "' $varLecture> ");
    $tpl->assign("tel", "<input type='text' name='f_tel' value='" . $row_tel . "' $varLecture> ");
    $tpl->assign("mail", "<input type='text' name='f_mail' value='" . $row_mail . "' $varLecture> ");
    $tpl->assign("dat", "<input type='text' name='f_dat' value='" . $row_dat . "' $varLecture> ");
     $tpl->assign("deb", "<input type='text' name='f_deb' value='" . $row_deb . "' $varLecture> ");
    $tpl->assign("dur", "<input type='text' name='f_dur' value='" . $row_dur . "' $varLecture> ");
     $tpl->assign("com", "<input type='text' name='f_com' value='" . $row_com . "' $varLecture> ");
}

function formulaire($varLecture, $tpl) {


//Assignation deis valeurs récupérées
    $tpl->assign("titreForm", "Fiche Seance en modification");
       $tpl->assign("id_1", "<input type='text' name='f_id_1' value='' $varLecture>");
    $tpl->assign("id", "<input type='text' name='f_id' value='' $varLecture>");
    $tpl->assign("deb", "<input type='text' name='f_deb' value='' $varLecture> ");
    $tpl->assign("dur", "<input type='text' name='f_dur' value='' $varLecture> ");
        $tpl->assign("com", "<input type='text' name='f_com' value='' $varLecture> ");
}

function detailSeanceModif($idRequete, $tpl, $varLecture2, $varLectureSeul) {
    $row = $idRequete->fetch();
       $row_id_1 = $row['idindividu_1'];
    $row_id = $row['idindividu'];
    $row_nom = $row['nom'];
    $row_dat = $row['dateseance'];
    $row_deb = $row['heuredebut'];
    $row_dur = $row['duree'];
       $row_com = $row['commentaire'];




//Assignation deis valeurs récupérées
    $tpl->assign("titreForm", "Fiche Seance en modification");
     $tpl->assign("id_1", "<input type='text' name='f_id_1' value='" . $row_id_1 . "' $varLectureSeul>");
    $tpl->assign("id", "<input type='text' name='f_id' value='" . $row_id . "' $varLectureSeul>");
    $tpl->assign("nom", "<input type='text' name='f_nom' value='" . $row_nom . "' $varLectureSeul>");
    $tpl->assign("dat", "<input type='text' name='f_dat' value='" . $row_dat . "' $varLecture2> ");
      $tpl->assign("deb", "<input type='text' name='f_deb' value='" . $row_deb . "' $varLecture2> ");
    $tpl->assign("dur", "<input type='text' name='f_dur' value='" . $row_dur . "' $varLecture2> ");
        $tpl->assign("com", "<input type='text' name='f_com' value='" . $row_com . "' $varLecture2> ");
}
