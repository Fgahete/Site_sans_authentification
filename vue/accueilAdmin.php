<?php
require_once ('include/libs/smarty.class.php');
$tpl = new Smarty();
// Chargement des données
if (isset($parametre['action'])) {

    $action = $parametre['action'];
    $tpl->assign("action", $action);
    switch ($action) {
        case 'consulter':
            $varLecture = " readonly='readonly' ";

            detailIndividu($idRequete, $tpl, $varLecture);
            $tpl->assign("action", $action);
            $tpl->assign("btnAction", "");

            $tpl->display('Vue/accueilAdminFiche.tpl');

            break;
        case 'ajouter':
            $varLecture = "";
            formulaire($varLecture, $tpl);
            $tpl->assign("action", 'validationAjout');
            $tpl->assign("btnAction", "<input type='submit' name='Valider' value='Valider'>");
            $tpl->display('Vue/accueilAdminFiche.tpl');

            break;

        case 'modifier':
            $varLecture2 = "";
            $varLectureSeul = " readonly='readonly' ";

            detailIndividuModif($idRequete, $tpl, $varLecture2, $varLectureSeul);
            $tpl->assign("action", 'validationModif');
            $tpl->assign("btnAction", "<input type='submit' name='Valider' value='Valider'>");
            $tpl->display('Vue/accueilAdminFiche.tpl');

            break;
        case 'supprimer':
            $varLecture = " readonly='readonly' ";

            detailIndividu($idRequete, $tpl, $varLecture);
            $tpl->assign("action", 'validationSuppr');
            $tpl->assign("btnAction", "<input type='submit' name='supprimer' value='Supprimer'");

            $tpl->display('Vue/accueilAdminFiche.tpl');

            break;
    }
} else {
$accueilAdmin = array();
$i = 0;
//pas de paramètre dans fetch() ==> tableau associatif. Pour deis nombres PDO::FETCH_NUM



while ($row = $idRequete->fetch()) {
    $accueilAdmin[$i]['idindividu'] = $row['idindividu'];
    $accueilAdmin[$i]['idfonction'] = $row['idfonction'];
    $accueilAdmin[$i]['nom'] = $row['nom'];
    $accueilAdmin[$i]['prenom'] = $row['prenom'];
    $i++;
}

$tpl->assign("libidindividu", "");
$tpl->assign("libidfonction", "");
$tpl->assign("libnom", "");
$tpl->assign("libprenom", "");
/* $nbLig = $idRequete->rowCount(); */
$tpl->assign('titreForm', 'Liste des séances');
$tpl->assign('accueilAdmin', $accueilAdmin);
$tpl->display('vue/accueilAdmin.tpl');
}



function detailIndividu($idRequete, $tpl, $varLecture) {
    $row = $idRequete->fetch();
    $row_id = $row['idindividu'];
    $row_phot = $row['photos'];
    $row_nom = $row['nom'];
    $row_pre = $row['prenom'];
    $row_tel = $row['telephone'];
    $row_mail = $row['mail'];
    $row_adr = $row['adresse'];
    $row_cp = $row['cp'];
    $row_vil = $row['ville'];



//Assignation deis valeurs récupérées
    $tpl->assign("titreForm", "Fiche Individu en modification");
    $tpl->assign("id", "<input type='text' name='f_id' value='" . $row_id . "' $varLecture>");
    $tpl->assign("phot", "<input type='image' name='f_phot' value='" . $row_phot . "' $varLecture> ");
    $tpl->assign("nom", "<input type='text' name='f_nom' value='" . $row_nom . "' $varLecture> ");
    $tpl->assign("pre", "<input type='text' name='f_pre' value='" . $row_pre . "' $varLecture> ");
    $tpl->assign("tel", "<input type='text' name='f_tel' value='" . $row_tel . "' $varLecture> ");
    $tpl->assign("mail", "<input type='text' name='f_mail' value='" . $row_mail . "' $varLecture> ");
    $tpl->assign("adr", "<input type='text' name='f_adr' value='" . $row_adr . "' $varLecture> ");
     $tpl->assign("cp", "<input type='text' name='f_cp' value='" . $row_cp . "' $varLecture> ");
    $tpl->assign("vil", "<input type='text' name='f_vil' value='" . $row_vil . "' $varLecture> ");
}

function formulaire($varLecture, $tpl) {


//Assignation deis valeurs récupérées
    $tpl->assign("titreForm", "Fiche Individu en modification");
    $tpl->assign("id", "<input type='text' name='f_id' value='' $varLecture>");
    $tpl->assign("phot", "<input type='image' name='f_phot' value='' $varLecture> ");
    $tpl->assign("nom", "<input type='text' name='f_nom' value='' $varLecture> ");
    $tpl->assign("pre", "<input type='text' name='f_pre' value='' $varLecture> ");
    $tpl->assign("tel", "<input type='text' name='f_tel' value='' $varLecture> ");
    $tpl->assign("mail", "<input type='text' name='f_mail' value='' $varLecture> ");
    $tpl->assign("adr", "<input type='text' name='f_adr' value='' $varLecture> ");
    $tpl->assign("cp", "<input type='text' name='f_cp' value='' $varLecture> ");
    $tpl->assign("vil", "<input type='text' name='f_vil' value='' $varLecture> ");
}

function detailIndividuModif($idRequete, $tpl, $varLecture2, $varLectureSeul) {
    $row = $idRequete->fetch();
    $row_id = $row['idindividu'];
    $row_phot = $row['photos'];
    $row_nom = $row['nom'];
    $row_pre = $row['prenom'];
   $row_tel = $row['telephone'];
    $row_mail = $row['mail'];
    $row_adr = $row['adresse'];
    $row_cp = $row['cp'];
    $row_vil = $row['ville'];




//Assignation deis valeurs récupérées
    $tpl->assign("titreForm", "Fiche Individu en modification");
    $tpl->assign("id", "<input type='text' name='f_id' value='" . $row_id . "' $varLectureSeul>");
    $tpl->assign("phot", "<input type='image' name='f_phot' value='" . $row_phot . "' $varLecture2> ");
    $tpl->assign("nom", "<input type='text' name='f_nom' value='" . $row_nom . "' $varLecture2> ");
    $tpl->assign("pre", "<input type='text' name='f_pre' value='" . $row_pre . "' $varLecture2> ");
    $tpl->assign("tel", "<input type='text' name='f_tel' value='" . $row_tel . "' $varLecture2> ");
    $tpl->assign("mail", "<input type='text' name='f_mail' value='" . $row_mail . "' $varLecture2> ");
    $tpl->assign("adr", "<input type='text' name='f_adr' value='" . $row_adr . "' $varLecture2> ");
      $tpl->assign("cp", "<input type='text' name='f_cp' value='" . $row_cp . "' $varLecture2> ");
    $tpl->assign("vil", "<input type='text' name='f_vil' value='" . $row_vil . "' $varLecture2> ");
}
