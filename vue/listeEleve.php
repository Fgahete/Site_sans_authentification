<?php

require_once('include/libs/smarty.class.php');
$tpl = new Smarty();
//chargement des données
$listeEleve = array();
$i = 0;
//pas de paramètre dans fetch() ==> tableau associatif. Pour des nombres PDO::FETCH_NUM
while ($row = $idRequete->fetch()) {
    $listeEleve[$i]['IDIndividu'] = $row['IDIndividu'];
    $listeEleve[$i]['Nom'] = $row['Nom'];
    $listeEleve[$i]['Prenom'] = $row['Prenom'];
    $listeEleve[$i]['NumeroTelephone'] = $row['NumeroTelephone'];
    $i++;
}

$tpl->assign("libIDIndividu", "");
$tpl->assign("libNom", "");
$tpl->assign("libPrenom", "");
$tpl->assign("libTelephone", "");

/* $nbLig = $idRequete->rowCount(); */
$tpl->assign('titreForm', 'Liste des Elève');
$tpl->assign('listeEleve', $listeEleve);
$tpl->display('vue/listeEleve.tpl');
