<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="template/style.css">
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="stylesheet" href="template/head&foot.css">
        <link rel="stylesheet" href="template/bootstrap.min.css">
        <link rel="icon" type="image/png" href="template/image/favicon.ico.png">
        <title>{$titreForm}</title>
    </head>
    <body>
        <header>
            <h1>
                Pixel Drool
                <img alt="Logo de l'auto-école Pixel Drool" src="template/image/logoPixelDrool.png" class="img-logo">
            </h1>
            <nav>
                <section class="lienacc">
                  <a href="index.php?gestion=accueilAdmin">Admin</a>
                <a href="index.php?gestion=accueilMoniteur">Moniteur</a>
                 <a href="index.php?gestion=accueilEleve">Eleve</a></section>
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="#">Liste des élèves</a>
                    </li>
                    <li>
                        <a href="#">Liste des séances</a>
                    </li>
                </ul>
                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control">
                        </div>
                    </form>
                    <button type="submit" class="btn btn-default">
                        Rechercher
                    </button>
            </nav>
        </header>

        <h3>{$titreForm}</h3>
        <div id="divTab"
             <form class="tableau" action="index.php" method="POST">

                <table>
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Début</th>
                            <th>Durée</th>
                            <th>Commentaire</th>
                            <th>Fonction</th>
                        </tr>

                    </thead>
                    {foreach from=$accueilEleve item=SeanceEleve}
                        <tbody>
                            <tr>
                                <td><span>{$libDateSeance}</span> {$SeanceEleve.DateSeance}</td>
                                <td><span>{$libHeureDebut}</span> {$SeanceEleve.HeureDebut}</td>
                                <td><span>{$libDuree}</span> {$SeanceEleve.Duree}</td>
                                <td><span>{$libCommentaire}</span> {$SeanceEleve.Commentaire}</td>
                                <td class="der">
                                    <form action="" method="GET">
                                        <input type="submit" name="consult" value="C" class="sub">
                                        <input type='hidden' name='consult' value='' class="sub">
                                    </form>
                                    <form action="" method="POST">
                                        <input type="submit" name="modif" value="M" class="sub">
                                        <input type='hidden' name='modif' value='' class="sub">
                                    </form>
                                    <form action="index.php" method="POST">
                                        <input type="submit" name="supp" onclick="return confirm('Merci de confirmer la suppression')" value="S" class="sub">
                                        <input type='hidden' name='supp' value="$accueilEleve['IDIndividu']" class="sub">
                                    </form>
                                    <!-- <form action="ajout.php" method="POST">
                                         <input type="submit" name="ajout"  value="+" class="sub">
                                         <input type='hidden' name='ajout' value='' class="sub">
                                     </form>-->
                                </td>
                            </tr>
                        </tbody>
                    {/foreach}
                </table>
            </form>
        </div>
        <form>
            <a href=""><input type="submit" name="Retour" value="Retour" class="clicc"></a>
        </form>

    </body>
</html>
