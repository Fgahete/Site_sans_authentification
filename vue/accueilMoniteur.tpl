<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="template/style.css">
        <link rel="stylesheet" href="css/header.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="stylesheet" href="template/head&foot.css">
        <link rel="stylesheet" href="template/bootstrap.min.css">
        <link rel="icon" type="image/png" href="template/image/favicon.ico.png">
        <title>{$titreForm}</title>
    </head>
    <body>
        <header>
            <h1>
                Pixel Drool
                <img alt="Logo de l'auto-école Pixel Drool" src="template/image/logoPixelDrool.png" class="img-logo">
            </h1>
            <nav><section class="lienacc">
                 <a href="index.php?gestion=admin">Admin</a>
                <a href="index.php?gestion=moniteur">Moniteur</a>
                 <a href="index.php?gestion=eleve">Eleve</a></section>
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="#">Liste des élèves</a>
                    </li>
                    <li>
                        <a href="index.php?gestion=moniteur">Liste des séances</a>
                    </li>
                </ul>
                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control">
                        </div>
                    </form>
                    <button type="submit" class="btn btn-default">
                        Rechercher
                    </button>
            </nav>
        </header>

        <h3>{$titreForm}</h3>
        <form action='index.php' method='GET'>
                <input value="moniteur" name="gestion" type="hidden">
                <input value="ajouter" name="action" type="hidden">
                <input type='submit' name='ajouter' value='Ajouter'><br><br><br>
            </form>
        <div id="divTab"
             <form class="tableau" action="index.php" method="POST">

                <table>
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Début</th>
                            <th>Durée</th>
                            <th></th>
                        </tr>

                    </thead>
                    {foreach from=$accueilMoniteur item=moniteur}
                        <tbody>
                            <tr>
                                <td><span>{$libdateseance}</span> {$moniteur.dateseance}</td>
                                <td><span>{$libnom}</span> {$moniteur.nom}</td>
                                <td><span>{$libprenom}</span> {$moniteur.prenom}</td>
                                <td><span>{$libheuredebut}</span> {$moniteur.heuredebut}</td>
                                <td><span>{$libduree}</span> {$moniteur.duree}</td>
                               <td class="der">
                                     <form class="enligne" action="index.php" method="POST">
                           <input value="{$moniteur.idseance}" name="ids" type="hidden">
                            <input value="{$moniteur.idindividu}" name="id" type="hidden">
                            <input value="moniteur" name="gestion" type="hidden">
                            <input value="consulter" name="action" type="hidden">
                            <input value="consulter" name="consulter" type="submit">
                        </form><br>
                        <form class="enligne" action="index.php" method="POST">
                              <input value="{$moniteur.idseance}" name="ids" type="hidden">
                            <input value="{$moniteur.idindividu}" name="id" type="hidden">
                            <input value="moniteur" name="gestion" type="hidden">
                            <input value="modifier" name="action" type="hidden">
                            <input value="modifier" name="modifier" type="submit">
                            </form><br>
                        
                        <form class="enligne" action="index.php" method="POST">
                              <input value="{$moniteur.idseance}" name="ids" type="hidden">
                            <input value="{$moniteur.idindividu}" name="id" type="hidden">
                            <input value="moniteur" name="gestion" type="hidden">
                            <input value="supprimer" name="action" type="hidden">
                            <input value="supprimer" name="supprimer" type="submit">
                        </form><br>
                                   
                                </td>
                            </tr>
                        </tbody>
                    {/foreach}
                </table>
            </form>
        </div>
        <form>
            <a href=""><input type="submit" name="Retour" value="Retour" class="clicc"></a>
        </form>

    </body>
</html>
