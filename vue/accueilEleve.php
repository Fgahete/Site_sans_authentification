<?php

require_once('include/libs/smarty.class.php');
$tpl = new Smarty();
//chargement des données
$accueilEleve = array();
$i = 0;
//pas de paramètre dans fetch() ==> tableau associatif. Pour des nombres PDO::FETCH_NUM
while ($row = $idRequete->fetch()) {
    $accueilEleve[$i]['DateSeance'] = $row['DateSeance'];
    $accueilEleve[$i]['HeureDebut'] = $row['HeureDebut'];
    $accueilEleve[$i]['Duree'] = $row['Duree'];
    $accueilEleve[$i]['Notation'] = $row['Notation'];
    $accueilEleve[$i]['Commentaire'] = $row['Commentaire'];
    $i++;
}

$tpl->assign("libDateSeance", "");
$tpl->assign("libHeureDebut", "");
$tpl->assign("libDuree", "");
$tpl->assign("libNotation", "");
$tpl->assign("libCommentaire", "");


/* $nbLig = $idRequete->rowCount(); */
$tpl->assign('titreForm', 'Accueil Eleve');
$tpl->assign('accueilEleve', $accueilEleve);
$tpl->display('vue/accueilEleve.tpl');
