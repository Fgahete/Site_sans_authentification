<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="template/style.css">
        <title>{$titreForm}</title>
    </head>
    <body>
        <h3>{$titreForm|upper}</h3>
        <form method="POST" action="index.php" enctype="multipart/form-data">
            <input value="client" name="gestion" type="hidden">
            <input value="{$action}" name="action" type="hidden">
            <table>
                {if $action neq 'consulter' && $action neq 'validationAjout'}
                    <tr>
                        <td>IDindividu</td><td>{$id}</td>
                    </tr>
                {/if}
                 <tr>
                    <td></td><td>{$phot}</td>
                </tr>
                <tr>
                    <td>Nom</td><td>{$nom}</td>
                </tr>
                <tr>
                    <td>Prenom</td><td>{$pre}</td>
                </tr>
                <tr>
                    <td>N°Téléphone</td><td>{$tel}</td>
                </tr>
                <tr>
                    <td>Mail</td><td>{$mail}</td>
                </tr>
                <tr>
                    <td>Adresse</td><td>{$adr}</td>
                </tr>
                <tr>
                    <td>Code Postale</td><td>{$cp}</td>
                </tr>
                 <tr>
                    <td>Ville</td><td>{$vil}</td>
                </tr>
                <tr>
                    <td><a href="index.php?gestion=admin" >Retour à la liste</a></td><td>{$btnAction}</td>
                </tr>
            </table>


        </form>
    </body>
</html>