<?php

require_once('modele/moniteurModele.php');

function lister() {
    $idRequete = listerSeanceMoniteur();
    require_once('vue/accueilMoniteur.php');
}
function consulter($parametre) {
    $idseance = $parametre['ids'];
    $idindividu = $parametre['id'];
    $idRequete = consulterSeance($idindividu, $idseance);

    require_once('vue/accueilMoniteur.php');
}

function modifier($parametre) {
     $idseance = $parametre['ids'];
      $idindividu = $parametre['id'];
    $idRequete = consulterSeance($idindividu, $idseance);

    require_once('vue/accueilMoniteur.php');
}

function supprimer($parametre) {
      $idseance = $parametre['ids'];
    $idindividu = $parametre['id'];
    $idRequete = consulterSeance($idindividu, $idseance);

    require_once('vue/accueilMoniteur.php');
}
function ajouter($parametre) {
    require_once('vue/accueilMoniteur.php');
}

function validationAjout($parametre) {
    $idRequete = insererSeance($parametre);
    lister();
}
function validationModif($parametre) {
    $idRequete = modifierSeance($parametre);
    lister();
}

function validationSuppr($parametre) {
    $idRequete = supprimerSeance($parametre);
    lister();
}