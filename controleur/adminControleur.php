<?php

require_once('modele/adminModele.php');

function lister() {
    $idRequete = listerAccueilAdmin();
    require_once('vue/accueilAdmin.php');
}
function consulter($parametre) {
    $idindividu = $parametre['id'];
    $idRequete = consulterIndividu($idindividu);

    require_once('vue/accueilAdmin.php');
}

function modifier($parametre) {
    $idindividu = $parametre['id'];
    $idRequete = consulterIndividu($idindividu);

    require_once('vue/accueilAdmin.php');
}

function supprimer($parametre) {
    $idindividu = $parametre['id'];
    $idRequete = consulterIndividu($idindividu);

    require_once('vue/accueilAdmin.php');
}
