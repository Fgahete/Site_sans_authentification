<?php

//Espace requêtes
require_once('modele.php');

function listerAccueilAdmin() {
    $cnx = getDB();
    $sql = "SELECT * FROM individu";
    $idResultat = executeRequete($cnx, $sql);
    return $idResultat;
}
function consulterIndividu($idindividu) {
    $cnx = getDB();
    $sql = "SELECT * FROM individu WHERE idindividu = ? ";
    $idRequete = executeRequete($cnx, $sql, array($idindividu));
    return $idRequete;
}

function insererIndividu($parametre) {
    $cnx = getBD();
    $idindividu = "";
    $photos = $parametre['f_phot'];
    $nom = $parametre['f_nom'];
    $prenom = $parametre['f_pre'];
    $telephone = $parametre['f_telephone'];
    $mail = $parametre['f_mail'];
    $adresse = $parametre['f_adr'];
    $cp = $parametre['f_cp'];
    $ville = $parametre['f_vil'];
    $sql = "INSERT INTO produit VALUES(?,?,?,?,?,?,?,?,?)";
    $idRequete = executeRequete($cnx, $sql, array($idindividu, $photos, $nom, $prenom, $mail, $telephone, $adresse, $cp, $ville));
    return $idRequete;
}

function modifierIndividu($parametre) {
    $cnx = getBD();
    $idindividu = $parametre['f_id'];
    $photos = $parametre['f_phot'];
    $nom = $parametre['f_nom'];
    $prenom = $parametre['f_pre'];
    $telephone = $parametre['f_tel'];
    $mail = $parametre['f_mail'];
    $adresse = $parametre['f_adr'];
    $cp = $parametre['f_cp'];
      $ville = $parametre['f_vil'];
    $sql = "UPDATE produit SET photos=? nom=?, prenom=?, telephone=?, mail=?, adresse=?, cp=?, ville=? WHERE idindividu=?";
    $idRequete = executeRequete($cnx, $sql, array($photos, $nom, $prenom, $telephone, $mail, $adresse, $cp, $ville, $idindividu));
    return $idRequete;
}

function supprimerIndividu($parametre) {
    $idindividu = $parametre['f_id'];
    $cnx = getBD();
    $sql = "DELETE FROM produit WHERE idindividu=?";
    $idRequete = executeRequete($cnx, $sql, array($idindividu));
    return $idRequete;
}
