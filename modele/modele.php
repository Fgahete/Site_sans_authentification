<?php

function executeRequete($cnx, $sql, $parametre = NULL) {
    if ($parametre == NULL) {
        $resultat = $cnx->query($sql);
        //requête simple
    } else {
        $resultat = $cnx->prepare($sql);
        $resultat->execute($parametre);
        //requête préparée
    }
    return $resultat;
}

function getDB() {
    $cnx = connexion(USER, SERVER, PW, DB);
    return $cnx;
}

function connexion($user, $srv, $pw, $db) {
    try {
        $cnx = new PDO('mysql:host=' . $srv . ';dbname=' . $db, $user, $pw, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8', PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        return $cnx;
    } catch (PDOException $e) {
        echo"Erreur d'acces au serveur MySQL:" . $e->getMessage();
        exit;
    }
}
