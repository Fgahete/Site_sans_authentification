<?php

//Espace requêtes
require_once('modele.php');

function listerSeanceMoniteur() {
    $cnx = getDB();
    $sql = "SELECT * FROM seance, individu WHERE seance.idindividu=individu.idindividu AND idindividu_1=2";
    $idResultat = executeRequete($cnx, $sql);
    return $idResultat;
}

function consulterSeance($idindividu, $idseance) {
    $cnx = getDB();
    $sql = "SELECT * FROM seance, individu WHERE individu.idindividu = ? AND seance.idseance = ?";
    $idRequete = executeRequete($cnx, $sql, array($idindividu, $idseance));
    return $idRequete;
}

function insererSeance($parametre) {
    $cnx = getBD();
    $idindividu = "";
    $dateseance = $parametre['f_dat'];
    $heuredebut = $parametre['f_deb'];
    $duree = $parametre['f_dur'];
    $idindividu_1 = $parametre['f_id_1'];
   
    $sql = "INSERT INTO seance VALUES(?,?,?,?,?)";
    $idRequete = executeRequete($cnx, $sql, array($idindividu, $dateseance, $heuredebut, $duree, $idindividu_1));
    return $idRequete;
}

function modifierSeance($parametre, $idseance) {
    $cnx = getBD();
    $dateseance = $parametre['f_dat'];
    $heuredebut = $parametre['f_deb'];
    $duree = $parametre['f_dur'];
    $idindividu_1 = $parametre['f_id_1'];
    
    $sql = "UPDATE seance SET dateseance=? heuredebut=?, duree=?, idindividu_1=? WHERE idseance = ?";
    $idRequete = executeRequete($cnx, $sql, array($dateseance, $heuredebut, $duree, $idindividu_1, $idseance));
    return $idRequete;
}

function supprimerSeance($parametre) {
    $idindividu = $parametre['f_id'];
    $cnx = getBD();
    $sql = "DELETE FROM seance WHERE idindividu=?";
    $idRequete = executeRequete($cnx, $sql, array($idindividu));
    return $idRequete;
}

