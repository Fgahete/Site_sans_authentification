
<?php

//Aucun code HTML!
// Connexion a PDO
function connexion($user, $pw, $srv, $bd) {
    try {
        $cnx = new PDO('mysql:host=' . $srv . ';dbname=' . $bd, $user, $pw, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8', PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        return $cnx;
    } catch (Exception $e) {
        echo "Erreur d'accès au serveur MySQL : " . $e->getMessage();
        exit;
    }
}
