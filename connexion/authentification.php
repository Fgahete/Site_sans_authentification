
<?php
require_once ('infoConnexion.php');
require_once ('connexion.php');
require_once ('executeSQL.php');
$cnx = connexion(UTILISATEUR, MDP, SERVEUR, BD);

if (isset($_POST['valider'])) {
    $login_temp = $_POST['f_login'];
    $Mdp_temp = $_POST['f_pw'];
    $sql = "SELECT IDIndividu, Login, Mdp FROM individu WHERE Login='$login_temp' AND Mdp='$Mdp_temp'";
    $idRequete = executeRequete($cnx, $sql);

    if ($idRequete->rowCount() == 1) {
        $row = $idRequete->fetch(PDO::FETCH_NUM);
        if ($jeton == $row[3]) {
            session_start();
            if (($_SESSION['Login'] = $login_temp) and ( $_SESSION['Mdp'] = $Mdp_temp)) {
                header('location:../index.php');
            } else if (($_SESSION['Login'] != $login_temp) and ( $_SESSION['Mdp'] != $Mdp_temp)) {
                echo("Votre identifiant ou mot de passe est erroné");
            }
        }
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pixel Drool</title>

        <meta name="description" content="Source code generated using layoutit.com">
        <meta name="author" content="LayoutIt!">

        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/header.css" rel="stylesheet">
        <link href="../css/footer.css" rel="stylesheet">

    </head>
    <body>
        <header>
            <h1 class="text-center">
                Pixel Drool
            </h1>
            <img alt="Logo pixel drool" src="../image/logov2.png">
        </header>

        <div class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
                <form action="authentification.php" method="POST">
                    Login: <br>
                    <input type="text" name="f_login" value=""><br>
                    Mot de passe : <br>
                    <input type="password" name="f_pw" value=""><br><br>

                    <input type="submit" name="valider" value="VALIDER">
                </form>

            </div>

            <div class="col-md-4">
            </div>
        </div>
        <footer>
            <div class="row">
                <div class="col-md-4">

                    <address>
                        <strong>Pixel drool Auto-école</strong> <br> 24 Place Gambetta<br> 36028, Châteauroux<br> Tel : (123) 456-7890
                    </address>
                </div>
                <div class="col-md-4">
                    <p>
                        2018 pixeldrool@mail.fr<br>
                        tout droits réservés
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        Site Map
                    </p>
                    <div>
                        ------------
                    </div>
                    <div>
                        -----------
                    </div>

                    <div>
                        ---------------
                    </div>
                    <div>
                        -------------
                    </div>

                </div>
            </div>
        </div>
    </footer>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
</body>
</html>
