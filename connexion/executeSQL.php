
<?php

// Execution de la requete
function executeRequete($cnx, $sql, $parametre = NULL) {
    //requete simple
    if ($parametre == NULL) {
        $resultat = $cnx->query($sql);
        //requete préparée
    } else {
        $resultat = $cnx->prepare($sql);
        $resultat->execute($parametre);
    }
    return $resultat;
}

?>
